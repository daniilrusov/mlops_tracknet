from setuptools import find_packages, setup

setup(
    name='src',
    packages=find_packages(),
    version='0.1.0',
    description='Homework for ITMO MLOPS course',
    author='Daniil Rusov',
    license='',
)
