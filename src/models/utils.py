import glob
import os
import numpy as np


def get_checkpoint_path(model_dir, version=None, checkpoint="latest"):
    """Function to get checkpoint of model in given directory.
    If it is needed to use not specific, but newest version of model,
    it can bee found automatically
    Arguments:
        model_dir (str): directory with model checkpoints
        version (str, None by default):
            name of directory with needed checkpoint.
            If 'latest', directory with maximum change time will be used
            If None, only model_dir and checkpoint will be used
        checkpoint (str, 'latest' by default): name of checkpoint (with .ckpt).
            If 'latest', checkpoint with maximum change time will be used
    """
    if version is not None:
        if version == "latest":
            list_of_files = glob.glob(f"{model_dir}/*")
            version = max(list_of_files, key=os.path.getmtime).split("/")[-1]
        model_dir = model_dir + "/" + version
    if checkpoint == "latest":
        list_of_files = glob.glob(f"{model_dir}/*.ckpt")
        checkpoint = max(list_of_files, key=os.path.getmtime).split("/")[-1]
    return f"{model_dir}/{checkpoint}"


def weights_update(model, checkpoint):
    model_dict = model.state_dict()
    pretrained_dict = checkpoint["state_dict"]
    real_dict = {}
    for k, v in model_dict.items():
        needed_key = None
        for pretr_key in pretrained_dict:
            if k in pretr_key:
                needed_key = pretr_key
                break
        assert needed_key is not None, "key %s not in pretrained_dict %r!" % (
            k,
            pretrained_dict.keys(),
        )
        real_dict[k] = pretrained_dict[needed_key]

    model.load_state_dict(real_dict)
    model.eval()
    return model


def load_data(input_dir, file_mask, n_samples=None):
    """Function to load input data for tracknet-like models training.
    Helps load prepared input of tracknet or classiier (like inputs,
    input_lengths, labels etc) or last station hits stored somewhere
    as npz-file (with info about positions and events).

    Arguments:
        input_dir (string): directory with files
        file_mask (string): mask to be applied to (f.e. *.npz)
        n_samples (int, None by default):
            Maximum number of samples to be loaded
    """
    flag = 0
    files = []
    data_merged = {}
    datafiles = glob.glob(f"{input_dir}/{file_mask}")
    for f in datafiles:
        one_file_data = np.load(f, mmap_mode="r", allow_pickle=True)
        first_file_len = len(one_file_data[one_file_data.files[0]])
        files = one_file_data.files
        for f in files:
            print(one_file_data[f][0])
        lengths = [len(one_file_data[f]) == first_file_len for f in files]
        assert all(lengths), "Lengths of files in npz are not equal!"
        if flag == 0:
            data_merged = dict(one_file_data.items())
            flag = 1
        else:
            for k, i in one_file_data.items():
                data_merged[k] = np.concatenate((data_merged[k], i), 0)
    if n_samples is None:
        n_samples = len(data_merged[files[0]])
    else:
        assert isinstance(n_samples, int), "n_samples must be int or None"
        n_samples = min(n_samples, len(data_merged[files[0]]))
    return {key: item[:n_samples] for key, item in data_merged.items()}
